//
//  CustomHeaderView.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import UIKit

protocol HeaderViewDelegate: AnyObject {
    func expandedSection(button: UIButton)
}

class CustomHeaderView: UITableViewHeaderFooterView {
    
    weak var delegate: HeaderViewDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
   
    func configure(title: StudentLevel, section: Int) {
        titleLabel.text = title.rawValue
        headerButton.tag = section
    }
    
    func rotateImage(_ expanded: Bool) {
        headerButton.tintColor = UIColor.black
        if expanded {
            headerButton.setImage(UIImage(systemName: "chevron.up"), for: .normal)
        } else {
            headerButton.setImage(UIImage(systemName: "chevron.down"), for: .normal)
        }
    }
    
    @IBAction func tapHeader(sender: UIButton) {
        delegate?.expandedSection(button: sender)
    }
}
