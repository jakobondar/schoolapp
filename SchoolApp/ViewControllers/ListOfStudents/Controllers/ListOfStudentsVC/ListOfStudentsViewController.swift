//
//  ListOfStudentsViewController.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import UIKit

class ListOfStudentsViewController: UIViewController {

    let listOfStudentsCellID = String(describing: ListOfStudentsTableViewCell.self)
    let headerID = String(describing: CustomHeaderView.self)
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: listOfStudentsCellID, bundle: nil), forCellReuseIdentifier: listOfStudentsCellID)
        tableView.register(UINib(nibName: headerID, bundle: nil), forHeaderFooterViewReuseIdentifier: headerID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        tableView.tableFooterView = UIView()
    }
}
