//
//  ListOfStudentsVC+Extensions.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import UIKit

//MARK: - add Table View
extension ListOfStudentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return SchoolManeger.shared.levelList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !SchoolManeger.shared.levelList[section].isExpanded {
            return 0
        }
        return SchoolManeger.shared.levelList[section].students.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listOfStudentsCellID, for: indexPath) as! ListOfStudentsTableViewCell
        cell.update(SchoolManeger.shared.levelList[indexPath.section].students[indexPath.row])
        return cell
    }
}

//MARK: - add Swipe Cell (student transfer)
extension ListOfStudentsViewController {
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            SchoolManeger.shared.studentGraduation(indexPath)
//            tableView.reloadData()
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
}

//MARK: - add Custom Header View
extension ListOfStudentsViewController: HeaderViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerID) as! CustomHeaderView
        header.configure(title: SchoolManeger.shared.levelList[section].title, section: section)
        header.rotateImage(SchoolManeger.shared.levelList[section].isExpanded)
        header.delegate = self
        return header
    }
    
    func expandedSection(button: UIButton) {
        let section = button.tag
        SchoolManeger.shared.levelList[section].isExpanded = !SchoolManeger.shared.levelList[section].isExpanded
//        tableView.reloadData()
        tableView.reloadSections(IndexSet(integer: section), with: .none)
    }
}
