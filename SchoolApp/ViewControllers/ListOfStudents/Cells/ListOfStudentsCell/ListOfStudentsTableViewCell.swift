//
//  ListOfStudentsTableViewCell.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import UIKit

class ListOfStudentsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameSurnameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func update(_ student: Student) {
        nameSurnameLabel.text = student.name + " " + student.surName
    }
}
