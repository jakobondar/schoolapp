//
//  GraduatesListTableViewCell.swift
//  SchoolApp
//
//  Created by Яков on 25.10.2021.
//

import UIKit

class GraduatesListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameSurnameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func update(_ student: Student) {
        nameSurnameLabel.text = student.name + " " + student.surName
        levelLabel.text = "Пройден " + student.level.rawValue + " курс"
    }
}
