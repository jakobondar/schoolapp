//
//  GraduatesListViewController.swift
//  SchoolApp
//
//  Created by Яков on 25.10.2021.
//

import UIKit

class GraduatesListViewController: UIViewController {

    let graduatesListCellID = String(describing: GraduatesListTableViewCell.self)
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: graduatesListCellID, bundle: nil), forCellReuseIdentifier: graduatesListCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}
