//
//  GraduatesListVC+Extensions.swift
//  SchoolApp
//
//  Created by Яков on 25.10.2021.
//

import UIKit

extension GraduatesListViewController: UITableViewDelegate, UITableViewDataSource {
    
//MARK: add Table View Cell
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SchoolManeger.shared.graduatesList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 35))
        let label = UILabel(frame: view.frame)
        label.backgroundColor = .systemBlue
        label.font = UIFont.boldSystemFont(ofSize:UIFont.labelFontSize)
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.text = "Выпускники:"
        label.textColor = .white
        label.textAlignment = .center
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: graduatesListCellID, for: indexPath) as! GraduatesListTableViewCell
        cell.update(SchoolManeger.shared.graduatesList[indexPath.row])
        return cell
    }
}
