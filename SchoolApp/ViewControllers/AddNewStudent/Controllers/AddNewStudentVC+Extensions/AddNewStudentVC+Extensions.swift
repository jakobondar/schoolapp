//
//  AddNewStudentVC+Extensions.swift
//  SchoolApp
//
//  Created by Яков on 28.10.2021.
//

import Foundation
import UIKit

//MARK: - function - Student verification and adding
extension AddNewStudentViewController {
    
    func addNewStudednt(_ name: UITextField, _ surname: UITextField) {

        if checkingTextFields(name, surname) && !nameVerification(name: name, surname: surname) {
            
            SchoolManeger.shared.generateNewStudent(name: nameTextField.text ?? "-",
                                                    surname: surnameTextField.text ?? "-",
                                                    level: .preparatory)
            goToTheListOfStudents()
        } else {
            errorMessage(nameVerification(name: name, surname: surname), checkingTextFields(name, surname))
        }
    }
    
    func nameVerification(name: UITextField, surname: UITextField) -> (Bool) {
        for level in SchoolManeger.shared.levelList {
            for student in level.students {
                if student.name == name.text && student.surName == surname.text {
                    return true
                }
            }
        }
        return false
    }
    
    func checkingTextFields(_ name: UITextField, _ surname: UITextField) -> (Bool) {

        if !(name.text == "") && !(surname.text == "") && !(name.text == nil) && !(surname.text == nil) {
            return true
        }
        return false
    }

    func goToTheListOfStudents() {
        tabBarController?.selectedIndex = 1
    }
    
    func errorMessage(_ errorNameTaken: Bool, _ errortextFieldIsEmpty: Bool) {

        if !errortextFieldIsEmpty {
            showAlert(title: "Ошибка", message: "Введите данные")

        } else if errorNameTaken {
            showAlert(title: "Ошибка", message: "Студент под таким именем уже существует")
            nameTextField.text = ""
            surnameTextField.text = ""
        }
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

//MARK: - function - activating the button next to the textField
extension AddNewStudentViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case nameTextField:
            surnameTextField.becomeFirstResponder()
        default:
            surnameTextField.resignFirstResponder()
            addNewStudednt(nameTextField, surnameTextField)
        }
        return true
    }
}
