//
//  AddNewStudentViewController.swift
//  SchoolApp
//
//  Created by Яков on 28.10.2021.
//

import UIKit

class AddNewStudentViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        surnameTextField.delegate = self
    }

    @IBAction func addNewStudentAction(_ sender: Any) {
        addNewStudednt(nameTextField, surnameTextField)
    }
}
