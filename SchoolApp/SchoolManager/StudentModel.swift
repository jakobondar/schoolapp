//
//  StudentModel.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import Foundation

enum StudentStatus: String {
    case learner = "Ученик"
    case graduate = "Выпускник"
}

enum StudentLevel: String {
    case preparatory = "Подготовительный"
    case base = "Базовый"
    case advanced = "Продвинутый"
}

struct Student {
    var idStudent: Int
    var name: String
    var surName: String
    var level: StudentLevel
    var status: StudentStatus
}
