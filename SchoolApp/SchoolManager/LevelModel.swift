//
//  LevelModel.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import Foundation

struct Level {
    var isExpanded: Bool = true
    var title: StudentLevel
    var students: [Student]
}
