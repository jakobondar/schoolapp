//
//  SchoolManager.swift
//  SchoolApp
//
//  Created by Яков on 24.10.2021.
//

import Foundation

class SchoolManeger {
    
    static let shared = SchoolManeger()
    
    var levelList = [Level]()
    var studentsList = [Student]()
    var graduatesList = [Student]()
    
    var listOfPreparatorylearner = [Student]()
    var listOfBaselearner = [Student]()
    var listOfAdvancedlearner = [Student]()
    
    private init () {
        studentsList = setStudentsList()
        listOfPreparatorylearner = studentsList.filter { $0.status == .learner }.filter { $0.level == .preparatory }.sorted(by: { $0.name < $1.name })
        listOfBaselearner = studentsList.filter { $0.status == .learner }.filter { $0.level == .base }.sorted(by: { $0.name < $1.name })
        listOfAdvancedlearner = studentsList.filter { $0.status == .learner }.filter { $0.level == .advanced }.sorted(by: { $0.name < $1.name })
        levelList = setLevelListlearners()
    }
    
    func setStudentsList() -> [Student] {
        
        let ivan = Student(idStudent: 1234, name: "Иван", surName: "Иванов", level: .preparatory, status: .learner)
        let petro = Student(idStudent: 2853, name: "Петр", surName: "Петренко", level: .base, status: .learner)
        let vasil = Student(idStudent: 3349, name: "Василий", surName: "Василенко", level: .advanced, status: .learner)
        let jakob = Student(idStudent: 8432, name: "Яков", surName: "Бондар", level: .advanced, status: .learner)
        let andrey = Student(idStudent: 3523, name: "Андрей", surName: "Воробей", level: .preparatory, status: .learner)
        let nikolay = Student(idStudent: 1010, name: "Николая", surName: "Картавый", level: .preparatory, status: .learner)
        let pavlo = Student(idStudent: 2245, name: "Павел", surName: "Павленко", level: .base, status: .learner)
        let anna = Student(idStudent: 3500, name: "Анна", surName: "Николаивна", level: .base, status: .learner)
        let elena = Student(idStudent: 1221, name: "Елена", surName: "Розумко", level: .base, status: .learner)
        let nazar = Student(idStudent: 9089, name: "Наразий", surName: "Лисак", level: .advanced, status: .learner)
        let alejandro = Student(idStudent: 5463, name: "Александра", surName: "Русаковская", level: .advanced, status: .learner)
        
        return [ivan, petro, vasil, jakob, andrey, nikolay, pavlo, anna, elena, nazar, alejandro]
    }
    
    func setLevelListlearners() -> [Level] {
        
        let preparatoryLevel = Level(title: .preparatory, students: listOfPreparatorylearner)
        let baseLevel = Level(title: .base, students: listOfBaselearner)
        let advancedLevel = Level(title: .advanced, students: listOfAdvancedlearner)
        
        return [preparatoryLevel, baseLevel, advancedLevel]
    }
    
    func studentGraduation(_ indexPath: IndexPath) {
        graduatesList.append(levelList[indexPath.section].students[indexPath.row])
        levelList[indexPath.section].students.remove(at: indexPath.row)
    }
    
    func generateNewStudent(name: String, surname: String, level: StudentLevel) {
        let generateID = Int.random(in: 1000...10000)
        
        switch level {
        case .preparatory:
            levelList[0].students.append(Student(idStudent: generateID, name: name, surName: surname, level: level, status: .learner))
        case .base:
            levelList[1].students.append(Student(idStudent: generateID, name: name, surName: surname, level: level, status: .learner))
        case .advanced:
            levelList[2].students.append(Student(idStudent: generateID, name: name, surName: surname, level: level, status: .learner))
        }
    }
}
